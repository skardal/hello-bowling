module Bowling where

import Data.Vector (Vector, snoc, empty, toList)
import Debug.Trace (trace)

data Frame = Empty | Partial Int  deriving Show

data Score = Regular Int Int | Spare | Strike deriving Show

makeThrow :: Frame -> Int -> Either Frame Score
makeThrow Empty 10 = Right Strike
makeThrow Empty pins = Left (Partial pins)
makeThrow (Partial n) pins = Right $ if n + pins == 10 then Spare else Regular n pins

data Game = Game { frame :: Frame  
                 , scores :: Vector Score
--                 , maxScore :: Int
--                 , currentScore :: Int
                 } deriving Show

newGame :: Game
newGame = Game { frame=Empty, scores=empty } -- maxScore = 300, currentScore = 0 }

playGame :: IO Game
playGame = do
  result <- progress initial
  return result
  where
    initial = newGame
    progress :: Game -> IO Game
    progress g = do
      putStrLn "Number of pins: "
      pins <- readLn :: IO Int
      let newG = case (makeThrow (frame g) pins) of
            Left frame' -> g { frame=frame' }
            Right score -> g { frame=Empty, scores=snoc (scores g) score }
      putStrLn $ show newG
      if length (scores newG) == 3
        then return newG
        else progress newG
      

scoreToInt :: Score -> Int
scoreToInt Strike = 10
scoreToInt Spare = 10
scoreToInt regular@(Regular first second) = let s = first + second
                                   in if s >= 0 && s <= 10
                                      then s
                                      else error $ "WTF IS THIS INPUT?? " ++ show regular

calculateScore :: Game -> Int
calculateScore g = trace (show $ scores g) processScores $ toList (scores g)
  where
    processScores [] = trace ("0") 0
    processScores org@(Strike:n:nn:_) =
      (processScores $ tail org) + scoreToInt Strike + scoreToInt n + scoreToInt nn
    processScores org@(Spare:n:_:_) =
      (processScores $ tail org) + scoreToInt Spare + scoreToInt n
    processScores (x:xs) = trace ("Got x=" ++ show x ++ " and xs=" ++ show xs) (scoreToInt x) + processScores xs
