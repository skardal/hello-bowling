module Main where

import Bowling

main :: IO ()
main = do
  g <- playGame  
  putStrLn "Done!!"
  let finalScore = show $ calculateScore g
  putStrLn "Calculating score..."
  putStrLn $ "Final score: " ++ finalScore
